package microservice.docs.controllers;

import microservice.docs.dao.UserDao;
import microservice.docs.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {

    UserDao userDao;
    @Autowired
    public UserController(@Qualifier("UserDaoArrayList") UserDao userDao) {
        this.userDao = userDao;
    }

    @PostMapping
    public ResponseEntity<?> addUser(@RequestParam("userId") String id) {
        return userDao.addUser(new User(UUID.fromString(id)));
    }


}

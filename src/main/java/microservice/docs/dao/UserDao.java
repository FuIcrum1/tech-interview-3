package microservice.docs.dao;

import microservice.docs.model.Document;
import microservice.docs.model.User;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface UserDao {
    ArrayList<User> users = new ArrayList<>();

    List<Document> getUserFiles(UUID userId);
    int addDocument(Document document, UUID userId);
    ResponseEntity<?> editDocument(Document newDocument, UUID userid, UUID documentId);
    ResponseEntity<?> deleteDocument(UUID userid, UUID documentId);

    String getFile(UUID userId, UUID documentId);
    default ResponseEntity<?> addUser(User user){
        if (!users.contains(user)){
            users.add(user);
            return ResponseEntity.ok(200);
        }
        return ResponseEntity.badRequest().body("User already exists");

    };
    default User findUser(UUID userId){
        for (User user : users) {
            if (user.equals(userId)) {
                return user;
            }
        }
        return null;
    };
}

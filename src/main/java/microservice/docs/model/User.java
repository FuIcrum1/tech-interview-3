package microservice.docs.model;

import org.springframework.http.ResponseEntity;

import java.io.File;
import java.util.*;

public class User {
    private UUID id;
    // A hashset will contain all name occurrences of the user's documents (to avoid duplicates and optimize the search)
    private final Set<String> documentNamesOccurrences;
    private final List<Document> documents;

    public User(UUID id) {
        this.id = id;
        this.documents = new LinkedList<>();
        this.documentNamesOccurrences = new HashSet<>();
    }

    public UUID getId() {
        return id;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public int addDocument(Document document) {
        int resultOfAddingOccurrence = addOccurrence(document);
        if (resultOfAddingOccurrence == 200) {
            documents.add(document);
        }
        return resultOfAddingOccurrence;
    }
    public ResponseEntity<?> editDocument(Document newDocument, UUID documentId) {

        if (documentNamesOccurrences.contains(newDocument.getName())) {
            return ResponseEntity.badRequest().body("Document with that name already exists");
        }
        Document documentToChange = findDocument(documentId);
        if (documentToChange == null) {
            return ResponseEntity.badRequest().body("Document does not exist");
        }
        deleteOccurrence(documentToChange);
        documentToChange.editDocument(newDocument);
        addOccurrence(documentToChange);
        return ResponseEntity.ok(200);
    }


    public String getFile(UUID documentId) {
        return findDocument(documentId).getFilename();
    }
    public ResponseEntity<?> deleteDocument(UUID documentId) {
        // possible optimization with iterators, helps to utilize the linked list features
        Document deletedDocument = findDocument(documentId);
        if (deletedDocument == null) {
            return ResponseEntity.badRequest().body("Document does not exist");
        }
        documents.remove(deletedDocument);
        String userPath = System.getProperty("user.dir");
        File fileToDelete = new File(userPath +"\\src\\main\\resources\\documents\\"+ deletedDocument.getFilename());
        fileToDelete.delete();
        deleteOccurrence(deletedDocument);
        return ResponseEntity.ok(200);
    }

    public Document findDocument(UUID documentId) {
        for (Document document : documents) {
            if (document.equals(documentId)) {
                return document;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User otherUser) {
            return id.equals(otherUser.id);
        } else if (obj instanceof UUID otherId) {
            return id.equals(otherId);
        }
        return false;
    }
    public void deleteOccurrence(Document document){
        documentNamesOccurrences.remove(document.getName());
    }

    public void deleteOccurrence(String name){
        documentNamesOccurrences.remove(name);
    }
    public int addOccurrence(Document document){
        if (documentNamesOccurrences.contains(document.getName())) {
            return 400;
        }
        documentNamesOccurrences.add(document.getName());
        return 200;
    }
}
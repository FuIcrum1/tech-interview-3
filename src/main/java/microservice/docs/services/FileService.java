package microservice.docs.services;

import microservice.docs.dao.UserDao;
import microservice.docs.model.Document;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class FileService {
    private final UserDao userDao;
    public FileService(@Qualifier("UserDaoArrayList") UserDao userDao) {
        this.userDao = userDao;
    }

    public ResponseEntity<?> uploadFile(MultipartFile file, UUID userId, Document document) throws IOException {
        switch (userDao.addDocument(document, userId)) {
            case 200:
                String userPath = System.getProperty("user.dir");
                file.transferTo(new File(userPath + "\\src\\main\\resources\\documents\\" + document.getFilename()));
                return ResponseEntity.ok(201);
            case 400:
                return ResponseEntity.badRequest().body("File and user id must not be empty");
            default:
                return ResponseEntity.badRequest().body("Something went wrong");
        }
    }
    public ResponseEntity<?> editDocument(Document newDocument, UUID userId, UUID documentId) {
        return userDao.editDocument(newDocument, userId, documentId);
    }
    public ResponseEntity<?> deleteDocument(UUID userid, UUID documentId) {
        return userDao.deleteDocument(userid, documentId);
    }

    public List<Document> getUserFiles(UUID userId) {
        return userDao.getUserFiles(userId);
    }
    public String getFile(UUID userId, UUID documentId) {
        return userDao.getFile(userId, documentId);
    }

}

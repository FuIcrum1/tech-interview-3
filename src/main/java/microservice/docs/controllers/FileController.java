package microservice.docs.controllers;


import microservice.docs.model.Document;
import microservice.docs.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
public class FileController {

    FileService fileService;
    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping
    public ResponseEntity<?> uploadFileTest(
            @RequestParam("file") MultipartFile file,
            @RequestParam("userId") String userId,
            @RequestParam("content") String content
    ) throws IOException {
        if (file == null || userId == null) {
            return ResponseEntity.badRequest().body("File and user id must not be empty");
        }
        Document newDocument = new Document(file.getOriginalFilename(), content);
        return fileService.uploadFile(file, UUID.fromString(userId), newDocument);
    }

    @PutMapping(path = "/{userId}/{documentId}")
    public ResponseEntity<?> editDocument(
            @RequestParam("name") String newName,
            @RequestParam("content") String newContent,
            @PathVariable("userId") String userId,
            @PathVariable("documentId") String documentId
    ) {
        Document newDocument = new Document(newName, newContent);
        fileService.editDocument(newDocument, UUID.fromString(userId), UUID.fromString(documentId));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{userId}/{documentId}")
    public ResponseEntity<?> deleteDocument(
            @PathVariable("userId") String userId,
            @PathVariable("documentId") String documentId
    ) {
          return fileService.deleteDocument(UUID.fromString(userId), UUID.fromString(documentId));
    }

    @GetMapping(path = "/{userId}/{documentId}")
    public FileSystemResource getFile(@PathVariable("userId") String userId,@PathVariable("documentId") String documentId) {
        try{
            return new FileSystemResource("src/main/resources/documents/" + fileService.getFile(UUID.fromString(userId), UUID.fromString(documentId)));
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping(path = "user/{userId}")
    public List<Document> getUserFiles(@PathVariable("userId") String userId) {
        return fileService.getUserFiles(UUID.fromString(userId));
    }

}

package microservice.docs.model;

import java.util.UUID;

public class Document {
    private String name, content;
    private final String filename, extension;
    private final UUID id;
    public Document(String name, String content) {
        this.id = UUID.randomUUID();
        this.content = content;
        String[] splitName = name.split("\\.");
        //TODO fix later
        this.name = splitName[0];
        extension = splitName.length > 1 ? splitName[splitName.length - 1] : "";
        this.filename = id + "." + extension;
    }
    public String getName() {
        if (extension.equals("")) {
            return name;
        }
        return name + "." + extension;
    }

    public String getNameWithoutExtension() {
        return name;
    }
    public UUID getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    public void editDocument(Document newDocument) {
        this.name = newDocument.getNameWithoutExtension() == null ? this.name : newDocument.getNameWithoutExtension();
        this.content = newDocument.getContent() == null ? this.content : newDocument.getContent();
    }

    public String getContent() {
        return content;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Document otherDocument) {
            return id.equals(otherDocument.getId());
        } else if (obj instanceof UUID otherId) {
            return id.equals(otherId);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Document{" +
                "name='" + name + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}

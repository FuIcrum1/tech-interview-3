package microservice.docs.services;

import microservice.docs.dao.UserDao;
import microservice.docs.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserDao userDao;


    @Autowired
    public UserService(@Qualifier("UserDaoArrayList") UserDao userDao) {
        this.userDao = userDao;
    }

    public ResponseEntity<?> addUser(User user) {
        return userDao.addUser(user);
    }
}

package microservice.docs.dao;

import microservice.docs.model.Document;
import microservice.docs.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.List;
import java.util.UUID;

//Most basic implementation, with more data about type of load on the service, I can create more optimized
// implementations
@Repository("UserDaoArrayList")
public class UserDas implements UserDao{
    @Override
    public int addDocument(Document document, UUID userId) {
        User user = findUser(userId);
        if (user == null) {
            user = new User(userId);
            users.add(user);
        }
        return user.addDocument(document);
    }
    @Override
    public String getFile(UUID userId, UUID documentId) {
        User user = findUser(userId);
        if (user == null) {
            return null;
        }
        return user.getFile(documentId);
    }

    @Override
    public List<Document> getUserFiles(UUID userId) {
        return findUser(userId).getDocuments();
    }

    @Override
    public ResponseEntity<?> editDocument(Document newDocument, UUID userid, UUID documentId) {
        return findUser(userid).editDocument(newDocument, documentId);
    }

    @Override
    public ResponseEntity<?> deleteDocument(UUID userid, UUID documentId) {
        return findUser(userid).deleteDocument(documentId);
    }

}